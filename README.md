# M17UF3R1-KohanAgustin-Survival
##Controles

- Moverse: WASD
- Correr: Ctrl (Solo en movimiento)
- Dash: Z (Solo en movimiento)
- Saltar: Barra Espaciadora
- Agacharse: Shift
- Interactuar: Click Izquierdo
- Apuntar: Click Derecho
- Disparar: Click Izquierdo (Solo Apuntando)
- Bailar: 1

Condición de derrota: Perder todos los puntos de vida.  
Condición de victoria: Obtener 4 objetos y cruzar la puerta de la montaña.
