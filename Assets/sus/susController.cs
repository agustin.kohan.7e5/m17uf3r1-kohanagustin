using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class susController : MonoBehaviour, IDamageable
{
    public enum ESusState
    {
        NONE = -1,
        PATROL = 0,
        CHASING = 1,
        ATTACKING = 2,
        WAITING = 3,
        DIE = 4
    }

    [SerializeField] Collider _bodyCollider;
    [SerializeField] Collider _chaseCollider;
    [SerializeField] Collider _attackCollider;

    [Space]
    [SerializeField] private Transform[] _rail;
    [SerializeField] private NavMeshAgent navMeshAgent;
    private int _currentNode = 0;
    
    [Space]
    [SerializeField] susEnemySO _susEnemySO;

    [Space]
    [SerializeField] Animator _anim;
    // Start is called before the first frame update
    void Awake()
    {
        SetSOValues();
    }
    private void Start()
    {
        ChangeState(ESusState.PATROL);
        navMeshAgent.speed = _susEnemySO.CurrentSpeed;
    }

    private void Update()
    {
        Move();
    }

    private void SetSOValues()
    {
        susEnemySO enemyDataAuxiliar = (susEnemySO)ScriptableObject.CreateInstance(typeof(susEnemySO));
        enemyDataAuxiliar.SetValues(_susEnemySO);
        _susEnemySO = enemyDataAuxiliar;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (_susEnemySO.eSusState == ESusState.PATROL)
                ChangeState(ESusState.CHASING);

            else if (_susEnemySO.eSusState == ESusState.ATTACKING)
                if (other.gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
                    damageable.TakeDamage(_susEnemySO.CurrentDamage);
        }
    }

    private void Move()
    {
        if (_susEnemySO.eSusState == ESusState.PATROL)
        {
            if (new Vector2(transform.position.x, transform.position.z) - new Vector2(_rail[_currentNode].position.x, _rail[_currentNode].position.z) == Vector2.zero)
            {
                if (_currentNode == _rail.Length-1)
                    _currentNode = 0;
                else
                    _currentNode++;
                ChangeState(ESusState.WAITING);
            }
        }
    }

    private void ChangeState(ESusState newESusState)
    {
        _susEnemySO.eSusState = newESusState;
        //Debug.Log(_susEnemySO.eSusState);
        switch (_susEnemySO.eSusState)
        {
            case ESusState.NONE:
                break;
            case ESusState.PATROL:
                _chaseCollider.enabled = true;
                _attackCollider.enabled = false;
                _anim.SetTrigger("Run");
                navMeshAgent.destination = _rail[_currentNode].position;
                break;
            case ESusState.CHASING:
                StopAllCoroutines();
                _chaseCollider.enabled = false;
                _anim.SetTrigger("Run");
                StartCoroutine(ChasePlayer());
                break;
            case ESusState.ATTACKING:
                _anim.SetTrigger("Attack");
                StartCoroutine(Attack());
                break;
            case ESusState.WAITING:
                _anim.SetTrigger("Idle");
                StartCoroutine(WaitAndChangeState(1f, ESusState.PATROL));
                break;
            case ESusState.DIE: 
                StopAllCoroutines();
                _anim.SetTrigger("Die");
                StartCoroutine(Die());
                break;
            default:
                break;
        }
    }

    
    private IEnumerator Attack()
    {
        _attackCollider.enabled = true;
        navMeshAgent.destination = transform.position;
        yield return new WaitForSeconds(AnimationLenght("susAttack"));
        _attackCollider.enabled = false;
        ChangeState(ESusState.CHASING);
    }

    private IEnumerator WaitAndChangeState(float seconds, ESusState newESusState)
    {
        navMeshAgent.destination = transform.position;
        yield return new WaitForSeconds(seconds);
        ChangeState(newESusState);
    }

    private IEnumerator ChasePlayer()
    {
        navMeshAgent.destination = GameManager.Instance.PlayerChasingNode.position;

        yield return null;

        if ((new Vector2(GameManager.Instance.PlayerChasingNode.position.x, GameManager.Instance.PlayerChasingNode.position.z) - new Vector2(transform.position.x, transform.position.z)).magnitude > _susEnemySO.ChasingRadio)
            StartCoroutine(WaitAndChangeState(1f, ESusState.PATROL));

        else if ((new Vector2(GameManager.Instance.PlayerChasingNode.position.x, GameManager.Instance.PlayerChasingNode.position.z) - new Vector2(transform.position.x, transform.position.z)).magnitude < ((CapsuleCollider)_attackCollider).height)
            StartCoroutine(WaitAndChangeState(_susEnemySO.AttackDelay, ESusState.ATTACKING));

        else
            StartCoroutine(ChasePlayer());
    }

    public void TakeDamage(float damageTaken)
    {
        _susEnemySO.CurrentHealth -= damageTaken;
        if (_susEnemySO.CurrentHealth <= 0)
            ChangeState(ESusState.DIE);
    }

    public IEnumerator Die()
    {
        _bodyCollider.enabled = false;
        _attackCollider.enabled = false;
        _chaseCollider.enabled = false;
        navMeshAgent.destination = transform.position;
        yield return new WaitForSeconds(AnimationLenght("susDie"));
        Destroy(gameObject);
    }

    private float AnimationLenght(string animationName)
    {
        foreach (var clip in _anim.runtimeAnimatorController.animationClips)
            if (clip.name == animationName)
                return clip.length;
        return 0f;
    }

}
