using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class susNavMesh : MonoBehaviour
{

    [SerializeField] private Transform[] rail;
    private int currentNode = 0;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private susController susController;

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("NavNode"))
            currentNode++;
    }

    private void Patrol()
    {
        if (susController)
        {

        }
        navMeshAgent.destination = rail[currentNode].position;
    }
}
