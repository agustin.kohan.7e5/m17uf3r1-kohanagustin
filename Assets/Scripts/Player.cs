using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour, IDamageable
{
    public enum EPlayerMovementState
    {
        NONE        = -1,
        DEFAULT     = 0,
        WALKING     = 1,
        LOCKED      = 2
    }

    public enum EPlayerActionState
    {
        NONE        = 0,
        JUMPING     = 1,
        TAUNTING    = 2,
        AIMING      = 3,
        SHOOTING    = 4,
        CROUCHING   = 5,
        RUNING      = 6,
        DASHING     = 7,
        INTERACTING = 8
    }

    [SerializeField] float _curretHealth = 100;

    [Space]
    [SerializeField] CharacterController _characterController;
    private bool _canMove = true;
    private bool _canJump = true;
    private bool _canCrouch = true;
    private bool _canRun = false;
    private bool _canDash = true;

    [Space]
    [SerializeField] float _speed;
    Vector3 _movementInput;
    Vector3 _currentMovement;
    [SerializeField] float _jumpForce;
    [SerializeField] PlayerAnimation _playerAnimation;

    [Space]
    Vector2 _currentRotation;
    [SerializeField] GameObject _mainCamera, _aimCamera, _celebrationCamera;
    [SerializeField] float _aimCameraSensitivity;
    [SerializeField] GameObject _spineBone;


    [Space]
    //[SerializeField] private PlayerAnimation _playerAnimation;
    [SerializeField] EPlayerMovementState _playerMovementstate;
    [SerializeField] EPlayerActionState _playerActionState;

    float rotationFactor = 10f;

    [Space]
    [SerializeField] Transform _bulletSpawner;
    [SerializeField] GameObject _bulletPrefab;

    [Space]
    public Inventory Inventory;
    void Start()
    {
        _currentRotation = transform.localRotation.eulerAngles;
        _playerMovementstate = EPlayerMovementState.DEFAULT;
        _playerActionState = EPlayerActionState.NONE;

        SetCrouchValues();
    }

    private void Update()
    {
        CheckPlayerMovementState();
        CheckPlayerActionState();
        _playerAnimation.UpdateSpeedBlend(_characterController.velocity.magnitude / _speed);
    }

    private void FixedUpdate()
    {
        //Player normal movement
        _currentMovement = new Vector3(Mathf.Lerp(_currentMovement.x, _movementInput.x, Time.deltaTime), _currentMovement.y, Mathf.Lerp(_currentMovement.z, _movementInput.z, Time.deltaTime));
        _movementInput.y -= _characterController.isGrounded ? 0 : 9.81f * Time.deltaTime;

        _characterController.Move(_speed * Time.deltaTime * (_mainCamera.transform.TransformDirection(_currentMovement) + new Vector3(0, _movementInput.y, 0)));

        if (_movementInput.magnitude != 0 && _playerActionState != EPlayerActionState.AIMING)
            HandleRotationFreeMove();
    }

    private void HandleRotationFreeMove()
    {
        Vector3 positionLookAt;
        positionLookAt = new Vector3(_characterController.velocity.x, 0.0f, _characterController.velocity.z);
        if (positionLookAt != Vector3.zero)
        {            
            Quaternion currentRotation = transform.rotation;

            Quaternion targetRotation = Quaternion.LookRotation(positionLookAt);
            transform.rotation = Quaternion.Slerp(currentRotation, targetRotation, rotationFactor * Time.deltaTime);
        }
        
    }


    //Update the player acceleration values.
    public void OnMove(InputAction.CallbackContext context)
    {
        if (context.performed && _canMove)
        {
            _movementInput = new Vector3(context.ReadValue<Vector2>().x, 0, context.ReadValue<Vector2>().y);
            
            if (_movementInput.magnitude >= 0)
            {
                _playerMovementstate = EPlayerMovementState.WALKING;
                _playerAnimation.MovementAnimation(_playerMovementstate);
            }
        }
        
    }

    public void OnRun(InputAction.CallbackContext context)
    {
        if (context.performed && _canRun)
        {
            _speed = 5f;
            _playerActionState = EPlayerActionState.RUNING;
        }
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.performed && _canJump)
        {
            if(_characterController.isGrounded && _playerMovementstate != EPlayerMovementState.LOCKED)
                StartCoroutine(PrepareJump());
        }
    }

    IEnumerator PrepareJump()
    {
        _playerMovementstate = EPlayerMovementState.LOCKED;
        _playerAnimation.ActionAnimation(EPlayerActionState.JUMPING);
        float AnimationLenght = 0;
        foreach (var clip in _playerAnimation._anim.runtimeAnimatorController.animationClips)
        {
            if (clip.name == "Jump")
            {
                AnimationLenght = clip.length / 2;
                break;
            }
        }
        yield return new WaitForSeconds(AnimationLenght);
        _playerMovementstate = EPlayerMovementState.DEFAULT;
        Jump();
    }

    private void Jump()
    {
        _playerActionState = EPlayerActionState.JUMPING;
        _movementInput.y = _jumpForce;
        StartCoroutine(CheckGroundTouched());
    }

    public IEnumerator CheckGroundTouched()
    {
        yield return null;
        if (!_characterController.isGrounded)
            StartCoroutine(CheckGroundTouched());
        else
            _playerActionState = EPlayerActionState.NONE;
    }

    [Space]
    [Range(0, 1)] [SerializeField] float _crouchPorcentage;
    private float _standColliderHeight;
    private float _standColliderCenter;
    private float _crouchColliderHeight;
    private float _crouchColliderCenter;
    private void SetCrouchValues()
    {
        _standColliderHeight = _characterController.height;
        _standColliderCenter = _characterController.center.y;

        _crouchColliderHeight = _standColliderHeight * _crouchPorcentage;
        _crouchColliderCenter = _standColliderCenter * _crouchPorcentage;
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        if (context.performed && _canCrouch)
        {
            if (_playerActionState != EPlayerActionState.CROUCHING)
            {
                _playerActionState = EPlayerActionState.CROUCHING;
                _playerAnimation.ActionAnimation(_playerActionState);
                _characterController.height = _crouchColliderHeight;
                _characterController.center = new Vector3(_characterController.center.x, _crouchColliderCenter, _characterController.center.z);
                
            }
            else
            {
                _playerActionState = EPlayerActionState.NONE;
                _playerAnimation.ActionAnimation(_playerActionState);
                _characterController.height = _standColliderHeight;
                _characterController.center = new Vector3(_characterController.center.x, _standColliderCenter, _characterController.center.z);
            }
        }
    }

    public void OnAim(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _aimCamera.SetActive(true);
            if (_playerActionState == EPlayerActionState.NONE)
            {
                _playerActionState= EPlayerActionState.AIMING;
                _playerAnimation.ActionAnimation(_playerActionState);
            }
        }
        if (context.canceled)
        {
            _aimCamera.SetActive(false);
            _playerActionState= EPlayerActionState.NONE;
            _playerAnimation.ActionAnimation(_playerActionState);
        }
    }
    public void OnShoot(InputAction.CallbackContext context)
    {
        //If player crouching == false  && player aim == true
        if (context.performed)
        {
            if (_playerActionState == EPlayerActionState.AIMING)
            {
                Instantiate(_bulletPrefab, _bulletSpawner.transform.position, transform.rotation);
                _playerAnimation.ActionAnimation(EPlayerActionState.SHOOTING);
                //Dispersion + Random.Range(-1, 1)
            }
        }
    }
    public void OnUse(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (_playerMovementstate == EPlayerMovementState.DEFAULT)
            {
                StartCoroutine(Use());
            }
        }
    }
    private IEnumerator Use()
    {
        _playerMovementstate = EPlayerMovementState.LOCKED;
        yield return new WaitForSeconds(1f); // <== Set Animation Duration
        _playerMovementstate = EPlayerMovementState.DEFAULT;
    }

    [Space]
    [SerializeField] private float _dashForce;
    [SerializeField] private float _dashDuration;
    public void OnDash(InputAction.CallbackContext context)
    {
        if (context.performed && _canDash)
        {
            StartCoroutine(Dash());
        }
    }

    //Like a TP
    IEnumerator Dash()
    {
        _playerMovementstate = EPlayerMovementState.LOCKED;
        _characterController.Move(_speed * _dashForce * Time.deltaTime * (_mainCamera.transform.TransformDirection(_currentMovement) + new Vector3(0, _movementInput.y, 0)));
        yield return new WaitForSeconds(_dashDuration);
        _playerMovementstate = EPlayerMovementState.DEFAULT;
    }

    public void OnTaunt(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (_playerMovementstate == EPlayerMovementState.DEFAULT && _playerActionState == EPlayerActionState.NONE)
            {
                StartCoroutine(Taunt()); 
            }
        }   
    }
    private IEnumerator Taunt()
    {
        float AnimationLenght = 0;
        foreach (var clip in _playerAnimation._anim.runtimeAnimatorController.animationClips)
        {
            if (clip.name == "Victory")
            {
                AnimationLenght = clip.length;
                break;
            }
        }
        _celebrationCamera.SetActive(true);
        _playerMovementstate = EPlayerMovementState.LOCKED;
        _playerActionState = EPlayerActionState.TAUNTING;
        _playerAnimation.ActionAnimation(_playerActionState);
        yield return new WaitForSeconds(AnimationLenght);
        _celebrationCamera.SetActive(false);
        _playerMovementstate = EPlayerMovementState.DEFAULT;
        _playerActionState= EPlayerActionState.NONE;
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (_playerMovementstate == EPlayerMovementState.DEFAULT && _playerActionState == EPlayerActionState.NONE)
            {
                StartCoroutine(Interact());
            }
        }
    }

    private IEnumerator Interact()
    {
        float AnimationLenght = 0;
        foreach (var clip in _playerAnimation._anim.runtimeAnimatorController.animationClips)
        {
            if (clip.name == "Interact")
            {
                AnimationLenght = clip.length;
                break;
            }
        }

        _playerMovementstate = EPlayerMovementState.LOCKED;
        _playerActionState = EPlayerActionState.INTERACTING;
        _playerAnimation.ActionAnimation(_playerActionState);
        yield return new WaitForSeconds(AnimationLenght);
        _playerMovementstate = EPlayerMovementState.DEFAULT;
        _playerActionState = EPlayerActionState.NONE;
    }

    //Equals the player rotation to the camera rotation.
    public void OnLook(InputAction.CallbackContext context)
    {
        if (context.performed && _playerActionState == EPlayerActionState.AIMING)
        {
            _currentRotation += new Vector2(context.ReadValue<Vector2>().x, context.ReadValue<Vector2>().y) * Time.deltaTime * _aimCameraSensitivity;
            transform.rotation = Quaternion.Euler(0, _currentRotation.x, 0);
            
            /* How to override idle animation rotation?
                _currentRotation.y = Mathf.Clamp(_currentRotation.y, -80, 80);
                _spineBone.transform.rotation = Quaternion.Euler(_currentRotation.y, 0, 0);
            */
        }
    }

    public void CheckPlayerMovementState()
    {
        switch (_playerMovementstate)
        {
            case EPlayerMovementState.NONE:
                break;
            case EPlayerMovementState.LOCKED:
                _canMove = false;
                _canRun = false;
                _canJump = false;
                _canCrouch = false;
                _canDash = false;
                //_movementInput = Vector3.zero;
                break;
            case EPlayerMovementState.DEFAULT:
                _canMove = true;
                _canRun = false;
                _canJump = true;
                _canCrouch = true;
                _canDash = false;
                break;
            case EPlayerMovementState.WALKING:
                _canMove = true;
                _canRun = true;
                _canJump = true;
                _canCrouch = true;
                _canDash = true;
                if (_movementInput.magnitude == 0)
                {
                    _playerMovementstate = EPlayerMovementState.DEFAULT;
                    _playerAnimation.MovementAnimation(_playerMovementstate);
                }                
                break;
            default:
                break;
        }
    }

    public void CheckPlayerActionState()
    {
        switch (_playerActionState)
        {
            case EPlayerActionState.NONE:
                _speed = 5f;
                break;
            case EPlayerActionState.JUMPING:
                _canMove = true;
                _canRun = false;
                _canJump = false;
                _canCrouch = false;
                break;
            case EPlayerActionState.TAUNTING:
                _speed = 0f;
                _canMove = false;
                _canRun = false;
                _canJump = false;
                _canCrouch = false;
                _canDash = false;
                break;
            case EPlayerActionState.AIMING:
                _speed = 3f;
                _canMove = true;
                _canRun = false;
                _canJump = false;
                _canCrouch = false;
                _canDash = false;
                break;
            case EPlayerActionState.SHOOTING:
                _speed = 3f;
                _canMove = true;
                _canRun = false;
                _canJump = false;
                _canCrouch = false;
                _canDash = false;
                break;
            case EPlayerActionState.CROUCHING:
                _speed = 2f;
                _canMove = true;
                _canRun = false;
                _canJump = false;
                _canCrouch = true;
                _canDash = false;
                break;
            case EPlayerActionState.RUNING:
                _speed = 8f;
                _canMove = true;
                _canRun = true;
                _canJump = true;
                _canCrouch = true;
                _canDash = true;
                if (_movementInput.magnitude == 0)
                    _playerActionState = EPlayerActionState.NONE;
                break;
            default:
                break;

        }
    }

    private IEnumerator WaitForOtherInput()
    {
        yield return new WaitForSeconds(0.2f);
        if (_movementInput.magnitude == 0)
            _playerMovementstate = EPlayerMovementState.DEFAULT;
    }

    public void TakeDamage(float damageTaken)
    {
        _curretHealth -= damageTaken;
        if (_curretHealth <= 0)
        {
            GameManager.Instance.GameOver(false);
        }
    }
}
