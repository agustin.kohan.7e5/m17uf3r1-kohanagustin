using UnityEngine;

[CreateAssetMenu(fileName = "ItemDataSO", menuName = "ScriptableObjects/ItemDataSO", order = 1)]
public class ItemDataSO : ScriptableObject
{
    public string Name;
    public Sprite ItemSprite;
    public Mesh Mesh;
}