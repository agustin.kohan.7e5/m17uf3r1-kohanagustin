using UnityEngine;

[CreateAssetMenu(fileName = "SusEnemySO", menuName = "ScriptableObjects/SusEnemySO", order = 2)]
public class susEnemySO : ScriptableObject
{
    public float CurrentHealth;
    public susController.ESusState eSusState;
    public float CurrentSpeed;
    public float CurrentDamage;
    public float ChasingRadio;
    public float AttackDelay;

    public void SetValues(susEnemySO susEnemySOtoClone)
    {
        CurrentHealth = susEnemySOtoClone.CurrentHealth;
        eSusState = susEnemySOtoClone.eSusState;
        CurrentSpeed = susEnemySOtoClone.CurrentSpeed;
        CurrentDamage = susEnemySOtoClone.CurrentDamage;
        ChasingRadio = susEnemySOtoClone.ChasingRadio;
        AttackDelay = susEnemySOtoClone.AttackDelay;
    }
}
