using UnityEngine;

[CreateAssetMenu(fileName = "InventoryData", menuName = "ScriptableObjects/InventoryData", order = 0)]
public class Inventory : ScriptableObject
{
    public ItemDataSO[] ItemArray = new ItemDataSO[4];

    public void SetValues(Inventory inventoryToClone)
    {
        ItemArray[0] = inventoryToClone.ItemArray[0];
    }
    public void ClearInventory()
    {
        for (int i = 0; i < ItemArray.Length; i++)
            ItemArray[i] = null;
    }
}

