using UnityEngine;

public class DamageableObject : MonoBehaviour
{
    [SerializeField] float _damage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
        {
            damageable.TakeDamage(_damage);
        }
    }
}
