using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject HelpMenu;

    public void StartGame() 
    {
        SceneManager.LoadScene(1);
    }

    public void ReturnToMenu()
    {
        MainMenu.SetActive(true);
        HelpMenu.SetActive(false);
    }

    public void OpenHelpMenu()
    {
        MainMenu.SetActive(false);
        HelpMenu.SetActive(true);
    }
    public void OnApplicationQuit()
    {
        Application.Quit();
    }

}
