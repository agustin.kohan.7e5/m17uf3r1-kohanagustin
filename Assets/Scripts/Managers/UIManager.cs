using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }

    // Start is called before the first frame update
    void Start()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }

    [SerializeField] GameObject WinPanel;
    [SerializeField] GameObject LosePanel;

    [Space]
    [SerializeField] Image[] KeyItemImages;
    public void GameOver(bool Victory)
    {
        if (Victory)
        {
            WinPanel.SetActive(true);
        }
        else
        {
            LosePanel.SetActive(true);
        }
    }

    public void UpdateUIItems()
    {
        for (int i = 0; i < KeyItemImages.Length; i++)
        {
            KeyItemImages[i].enabled = true;
            if (GameManager.Instance.Player.Inventory.ItemArray[i] == null)
            {
                KeyItemImages[i].enabled = false;
            }
            else
            {
                KeyItemImages[i].sprite = GameManager.Instance.Player.Inventory.ItemArray[i].ItemSprite;
            }
        }
    }
}
