using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }
    public Player Player;
    public MeshFilter[] VisualPlayerItems;
    public PlayerInput PlayerPlayerInput;
    public Transform PlayerChasingNode;

    public void Start()
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        Player.Inventory.ClearInventory();
        UIManager.Instance.UpdateUIItems();
    }

    public void GameOver(bool Victory)
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        PlayerPlayerInput.enabled = false;

        UIManager.Instance.GameOver(Victory);
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(1);
        Destroy(gameObject);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
