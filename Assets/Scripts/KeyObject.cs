using UnityEngine;

public class KeyObject : Item, ITakeable
{
    public void OnTake(GameObject player)
    {
        var Inventory = player.GetComponent<Player>().Inventory.ItemArray;

        for (int i = 0; i < Inventory.Length; i++)
        {
            if (Inventory[i] == null)
            {
                Inventory[i] = _itemDataSO;
                UIManager.Instance.UpdateUIItems();
                GameManager.Instance.VisualPlayerItems[i].gameObject.SetActive(true);
                GameManager.Instance.VisualPlayerItems[i].mesh = _itemDataSO.Mesh;
                Destroy(this.gameObject);
                break;
            }
        }
    }
}
