using System.Collections;
using UnityEngine;
using static Player;

public class PlayerAnimation : MonoBehaviour
{
    public Animator _anim;


    public void MovementAnimation(EPlayerMovementState ePlayerMovementState)
    {
        switch (ePlayerMovementState)
        {
            case EPlayerMovementState.NONE:
                _anim.SetBool("IsMoving", false);
                break;
            case EPlayerMovementState.DEFAULT:
                _anim.SetBool("IsMoving", false);
                break;
            case EPlayerMovementState.WALKING:
                _anim.SetBool("IsMoving", true);
                break;
            case EPlayerMovementState.LOCKED:
                _anim.SetBool("IsMoving", false);
                break;
            default:
                break;
        }
    }

    public void UpdateSpeedBlend(float speed) => _anim.SetFloat("SpeedBlend", speed);

    public void ActionAnimation(EPlayerActionState ePlayerActionState)
    {
        switch (ePlayerActionState)
        {
            case EPlayerActionState.NONE:
                _anim.SetBool("IsCrouching", false);
                StartCoroutine(DecreaseLayerWeightProgresive("AimLayer"));
                break;
            case EPlayerActionState.JUMPING:
                _anim.SetTrigger("JumpTrigger");
                StartCoroutine(DecreaseLayerWeightProgresive("AimLayer"));
                break;
            case EPlayerActionState.TAUNTING:
                StartCoroutine(DecreaseLayerWeightProgresive("AimLayer"));
                _anim.SetTrigger("TauntTrigger");
                break;
            case EPlayerActionState.AIMING:
                StartCoroutine(IncreaseLayerWeightProgresive("AimLayer"));
                break;
            case EPlayerActionState.SHOOTING:
                _anim.SetTrigger("ShootTrigger");
                break;
            case EPlayerActionState.CROUCHING:
                _anim.SetBool("IsCrouching", true);
                StartCoroutine(DecreaseLayerWeightProgresive("AimLayer"));
                break;
            case EPlayerActionState.INTERACTING:
                _anim.SetTrigger("InteractTrigger");
                break;
            default:
                break;
        }
    }
   
    private IEnumerator DecreaseLayerWeightProgresive(string layerIndex)
    {
        if (_anim.GetLayerWeight(_anim.GetLayerIndex(layerIndex)) > 0)
        {
            _anim.SetLayerWeight(_anim.GetLayerIndex(layerIndex), _anim.GetLayerWeight(_anim.GetLayerIndex(layerIndex)) - 0.1f);
            yield return null;
            StartCoroutine(DecreaseLayerWeightProgresive(layerIndex));
        }
    }
   
    private IEnumerator IncreaseLayerWeightProgresive(string layerIndex)
    {
        if (_anim.GetLayerWeight(_anim.GetLayerIndex(layerIndex)) < 1)
        {
            _anim.SetLayerWeight(_anim.GetLayerIndex(layerIndex), _anim.GetLayerWeight(_anim.GetLayerIndex(layerIndex)) + 0.1f);
            yield return null;
            StartCoroutine(IncreaseLayerWeightProgresive(layerIndex));
        }
    }
}
