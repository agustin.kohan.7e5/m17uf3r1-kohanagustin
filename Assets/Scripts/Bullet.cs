using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Damage;
    public float BulletSpeed;
    [SerializeField]
    private float _timeLife;
    private float _timer = 0;

    protected virtual void Awake()
    {
        float z = transform.eulerAngles.z + 90;
        GetComponent<Rigidbody>().velocity = BulletSpeed * transform.TransformVector(Vector3.forward);
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= _timeLife) Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
            damageable.TakeDamage(Damage);

        Destroy(gameObject);
    }

}
