using TMPro;
using UnityEngine;

public class FinalDoor : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _collectText;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && GameManager.Instance.Player.Inventory.ItemArray[3] != null)
        {
            GameManager.Instance.GameOver(true);
        }
        else if (other.gameObject.CompareTag("Player") && GameManager.Instance.Player.Inventory.ItemArray[3] == null)
        {
            _collectText.gameObject.SetActive(true);
            _collectText.transform.position = Camera.main.WorldToScreenPoint(transform.position, Camera.MonoOrStereoscopicEye.Mono);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            _collectText.transform.position = Camera.main.WorldToScreenPoint(transform.position, Camera.MonoOrStereoscopicEye.Mono);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _collectText.gameObject.SetActive(false);
        }
    }
}
