using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [SerializeField]
    protected ItemDataSO _itemDataSO;

    private void Awake()
    {
        //gameObject.GetComponent<SpriteRenderer>().sprite = _itemDataSO.ItemSprite;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.TryGetComponent<ITakeable>(out ITakeable takeable) && other.gameObject.CompareTag("Player"))
            takeable.OnTake(other.gameObject);
    }
}
